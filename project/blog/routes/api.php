<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
// get
// Route::get("students","StudentController@index");

// Route::get("students/{id}","StudentController@show");
// // post
// Route::post("students","StudentController@store");

// // put
// Route::put("students/{id}","StudentController@edit");

// // patch
// Route::patch("students/{id}","StudentController@edit_patch");

// // delete
// Route::delete('students/{id}',"StudentController@destroy");

Route::resource('students', 'StudentController');
