<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Student;
use Psy\TabCompletion\Matcher\FunctionDefaultParametersMatcher;

function success($student){

    return response()->json([
        'status code' => '200',
        'message' => 'Resquest success',
        'data' => $student
    ],200);
}

class StudentController extends Controller
{
    public function index(){

        $student = Student::all();

        return success($student);
    }

    public function show($id){

        $student = Student::findOrFail($id);

        return success($student);
    }

    public function store(Request $request){

        $student = Student::create($request->all());

        return success($student);
    }

    public function update(Request $request,$id){

        $student = Student::findOrFail($id);

        $student->update($request->all());

        return success($student);
    }

    public function destroy($id){

        $student = Student::destroy($id);

        return response()->json([
            'code' => '200',
            'message' => 'Delete success'
        ],200);
    }
}
